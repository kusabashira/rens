Rens.zip: Rens
	7z a Rens.zip Rens
	rm -rf Rens

Rens: rens.exe
	mkdir Rens
	mv plug-ins Rens
	mv rens.exe Rens
	cp rens.nako Rens
	cp README.txt Rens
	cp Makefile Rens

rens.exe: rens.nako
	nakomake rens.nako -t cnako -p dnako -p nakofile
